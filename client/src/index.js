// @flow
/* eslint eqeqeq: "off" */


import * as React from 'react';
import { Component,sharedComponentData } from 'react-simplified';
import { HashRouter, Route, NavLink, Redirect,Switch } from 'react-router-dom';
import ReactDOM from 'react-dom';
import Popup from 'reactjs-popup';
var Base64 = require('js-base64').Base64;
import {
  Alert,
  ListGroup,
  NavBar,
  Oppsett,
  Input,
  Row,
  Column,
  ContainerFluid,
  Overskrift,
  ListGroupInline,
  CheckBox,
  Card
} from './widgets';
import { sakService,kategoriService, Sak, OpprettSak, OpprettKommentarer} from './services';

import createHashHistory from 'history/createHashHistory';
const history = createHashHistory(); // Use history.push(...) to programmatically change path

class Menu extends Component {
  kategoriListe = [];


  render() {
    return (
      <NavBar>
        <NavBar.Brand>Fake News</NavBar.Brand>
        <NavBar.LinkLeft to="/nyheter/Sport"> Sport</NavBar.LinkLeft>
        <NavBar.LinkLeft to="/nyheter/Kultur"> Kultur</NavBar.LinkLeft>
        <NavBar.LinkLeft to="/nyheter/Tech"> Tech</NavBar.LinkLeft>
        <NavBar.LinkRight to="/registrer"> Administrer nyhetssaker</NavBar.LinkRight>
      </NavBar>
    );
  }

  mounted(){
    kategoriService
      .getKategorier()
      .then(kategori => (this.kategoriListe = kategori))
      .catch((error: Error) => Alert.danger(error.message))
  }
}

class Footer extends Component{
  render(){
    return(
      <footer>
        <br/>
        <div className={"text-center"}>
          <p><strong>Kontakt: Albert@fakenews.com</strong></p>
        </div>
        <br/>
      </footer>
    )
  }
}

class Redirection extends Component{
  render() {
    return(
      <Redirect to="/nyheter"/>
    )
  }
}

class LiveFeed extends Component{
  nyesteArtikler = [];
  interval=null;

  render() {
    return(
      <marquee style={{background: "grey"}}>
        <ListGroupInline>
      {this.nyesteArtikler.map(artikkel =>(
        <ListGroupInline.Item key={artikkel.artikkelID} style={{color: "white"}} to={"/nyheter/"+artikkel.kategoriNavn+"/"+artikkel.artikkelID}>
          {artikkel.overskrift} {'   '}{artikkel.tidspunkt}
        </ListGroupInline.Item>
      ))}
        </ListGroupInline>
      </marquee>
    )
  }

  mounted() {
    sakService
      .getLiveFeed()
      .then(artikkel => (this.nyesteArtikler = artikkel))
      .catch((error: Error) => Alert.danger(error.message));

    this.interval = setInterval(() => {
      sakService
        .getLiveFeed()
        .then(artikkel => (this.nyesteArtikler = artikkel))
        .catch((error: Error) => Alert.danger(error.message));
    },9000);
  }

  beforeUnmount(){
    if(this.interval){
      clearInterval(this.interval);
    }
  }

}

class ForsideVisning extends Component{
  delt = {nyheter: []};
  sideNr = 1;
  sakNrStart = 0;
  antallArtikler = 0;
  antSider = 1;
  antallSakerPrSide = 9;



  render() {
    return (
      <div >
        <LiveFeed/>
        <br></br>
        <br></br>
        <Oppsett midtBredde={10} sideBredde={1} >
          <ContainerFluid>
            <Row  styles={{ margin: 'auto', justifyContent: 'center', alignItems: 'flex-start'}}>
              <div className={"card-columns"}>
              {this.delt.nyheter.map(e => (

                  <Card key={e.artikkelID} to={'nyheter/'+e.kategoriNavn+'/'+e.artikkelID} link={e.bildelink} title={e.overskrift} >
                    {e.antallLikesArtikkel}
                        {(e.antallLikesArtikkel === 1 ) ? (" like") : (" likes")}
                  </Card>
              ))}
              </div>
            </Row>
          </ContainerFluid>
          <br></br>
          <footer>
            <div className={"text-center"}>
              {(this.sakNrStart>0) ? (
                <button type="button" style={{padding: 0, border: "none", backgroundColor: "white"}} className="mr-4" onClick={this.tilForrigeSide} >
                  <span><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAflBMVEX///8zMzMdHR0gICDLy8u/v78qKiotLS0ICAgwMDAoKCgaGhoREREjIyMeHh4mJiYVFRX09PTh4eEDAwNISEh1dXXZ2dmenp6lpaX5+fnS0tLp6elTU1NjY2OMjIy2trZ8fHxdXV0+Pj6UlJRubm53d3c4ODiFhYWsrKxMTEwLGXaBAAADV0lEQVR4nO3b6XaiQBCGYRuQRSCRuMS4RqOT5P5vcBzMZAhC5Qc4farzPldQfcD66Op2MAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4L+aD4dz2zXc0MMpTn0/jWcPtiu5kZdJaEphvLVdyy2sprn55G9sl9O/XRaZijvnnuJxbL6auPVbnEej2gJNOLNdVJ9e4qS+QGMy21X1p3j3r9dnTOpMLg7HUdMCzV1gu7KezOotxrEVzs1Vi3HrLX1sajEOdZpifde6PhM+2y6vu2Ha3GIuJivb9XX2nAnrM+mj7fq6evh1Ly5Q/RfNQmgxxiSx9idY7D3pAY4i7UEReKG0wPHRdoFdvYotJsp2tgvsaHXIpQX679pT4u2bFvNiu8COnjap9ADD0dJ2hR0Fudhi0uOT7Qo72opvqAMtZiq2mHyqvcXUpoVXLUb99PAot5hc+4Z+GcotZqO9xTROCytv6JvtAjtaNU8L/7o/aJ9uyy3GZK+2C+yqdVpYCj3tLWaetE4L//D2he0KO5KmhWep9hZTrMUWY0aLZaDEsvFdC1oOJCpL9LRI4/frr+bh5Jv16ZJ4h9qH8yq2XVPfouzrgGzz3SuqT+IVTj/Cs7A6BVyIm0Gt4spP8SRuJrS6r8zi9+79DM+iykUmBxvNWXL4t8Kt+D2qVXWFQ+F4V6/qWzoQ5zJaVTvNYCEeoCmVfRlHrN3Li7B2J3QqnmMrlGT1PZQ8I1UnmVyfGz069HGa+EnTTDDIxClG4mvhZWbRsL6zlRH6TbIJhkoEwrHRXpjV5OpnpSXpYkK+1n5gUdoJI8Uw1D7SL8399o1Gkg1tl9eHQgr/TPv1iwsp/D03/h8jhf/ooP30oiSFf5Rqv0dTEsN/ov2M5kIK//Gz7ep6QfjbLq8P81wI/5jwV0IK/7ufEP7a7++VCH/b5fXiB4T/9qeHvxP9Rgx/34n9lBT+4d52df2YtYf/WPudxQ/t4R+p/3vXh/bwH9surS+t4T92op2WWsI/dWK3eNEc/g49w5bwT21X1auG8A9Ptovq13X4Z05soqpq4T9yJQ4rFtXwj4ztcm4hyD6T0XfjIONKcYzzMIpC35GRW5PV4rTfvO6c2P8CAAAAAAAAAAAAAAAAAAAAAAAAAPr0G6/bPLLO/+s1AAAAAElFTkSuQmCC" width="20" height="20"/></span>
                </button>) : (
                null
              )}
              Side {this.sideNr} av {this.antSider}
              {((this.sakNrStart+this.antallSakerPrSide)<this.antallArtikler) ? (
                <button type="button" style={{padding: 0, border: "none", backgroundColor: "white"}} className=" ml-4" onClick={this.tilNesteSide}>
                  <span><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRy4KlLkS5-4gBE8MPtJ-1tsb9qIze3LZXrwraZZHzQkF-Jnc_g"width="20" height="20"/></span>
                </button>) : (
                null
              )}
            </div>
          </footer>
        </Oppsett>
      </div>
    );
  }

  tilNesteSide(){
    this.sakNrStart+=this.antallSakerPrSide;
    this.sideNr++;
    sakService
      .getArtikler(this.sakNrStart)
      .then(nyheter => {
        this.delt.nyheter=nyheter;
        this.endreRekkefolge();
      })
      .catch((error: Error) => Alert.danger(error.message));
  }

  endreRekkefolge() {
    let mellomlager = this.delt.nyheter.filter((e, i) => i % 3 === 0);
    mellomlager = mellomlager.concat(this.delt.nyheter.filter((e, i) => ((i+2) % 3) === 0));
    mellomlager = mellomlager.concat(this.delt.nyheter.filter((e, i) => ((i+1) % 3) === 0));
    this.delt.nyheter = mellomlager;
  }
//Holdt på med
  tilForrigeSide(){
    this.sakNrStart-=this.antallSakerPrSide;
    this.sideNr--;
    sakService
      .getArtikler(this.sakNrStart)
      .then(nyheter => {
        this.delt.nyheter=nyheter;
        this.endreRekkefolge();
      })
      .catch((error: Error) => Alert.danger(error.message));
  }

  mounted(){
    sakService
      .getArtikler(this.sakNrStart)
      .then(nyheter => {
        this.delt.nyheter=nyheter;
        this.endreRekkefolge();
      })
      .catch((error: Error) => Alert.danger(error.message));

    sakService
      .getAntArtikler()
      .then(antall => {
        this.antallArtikler = antall[0].antall;
        this.antSider = Math.ceil(this.antallArtikler/this.antallSakerPrSide);
        console.log('Ant Artikler '+ antall[0].antall);
      })
      .catch((error: Error) => Alert.danger(error.message));
  }
}

class Kategori extends Component<{match: { params: { kategorinavn: string }}}>{
  delt = {kategori: []};
  antallSakerPrSide = 9;
  sideNr = 1;
  antSider = 1;
  antallArtikler = 0;
  artikkelNrStart = 0;

  render() {
    return (
      <div>
        <br></br>
        <h1 className="text-center">{this.props.match.params.kategorinavn}</h1>
        <br></br>
        <Oppsett midtBredde={8} sideBredde={2} >
          <ContainerFluid>
            <Row  styles={{maxWidth: 1200, margin: 'auto', justifyContent: 'center', alignItems: 'flex-start'}}>
              <div className={"card-columns"}>
                {this.delt.kategori.map(e => (
                  <Card key={e.artikkelID} exact={true} to={'/nyheter/'+e.kategoriNavn+'/'+e.artikkelID} link={e.bildelink} title={e.overskrift}>
                      {e.antallLikesArtikkel}
                      {(e.antallLikesArtikkel === 1 ) ? (" like") : (" likes")}
                  </Card>
                ))}
              </div>
            </Row>
          </ContainerFluid>
          <br></br>
          <footer>
            <div className={"text-center"}>
              {(this.artikkelNrStart>0) ? (
                <button type="button" style={{padding: 0, border: "none", backgroundColor: "white"}} className="mr-4" onClick={this.tilForrigeSide} >
                  <span><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAflBMVEX///8zMzMdHR0gICDLy8u/v78qKiotLS0ICAgwMDAoKCgaGhoREREjIyMeHh4mJiYVFRX09PTh4eEDAwNISEh1dXXZ2dmenp6lpaX5+fnS0tLp6elTU1NjY2OMjIy2trZ8fHxdXV0+Pj6UlJRubm53d3c4ODiFhYWsrKxMTEwLGXaBAAADV0lEQVR4nO3b6XaiQBCGYRuQRSCRuMS4RqOT5P5vcBzMZAhC5Qc4farzPldQfcD66Op2MAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4L+aD4dz2zXc0MMpTn0/jWcPtiu5kZdJaEphvLVdyy2sprn55G9sl9O/XRaZijvnnuJxbL6auPVbnEej2gJNOLNdVJ9e4qS+QGMy21X1p3j3r9dnTOpMLg7HUdMCzV1gu7KezOotxrEVzs1Vi3HrLX1sajEOdZpifde6PhM+2y6vu2Ha3GIuJivb9XX2nAnrM+mj7fq6evh1Ly5Q/RfNQmgxxiSx9idY7D3pAY4i7UEReKG0wPHRdoFdvYotJsp2tgvsaHXIpQX679pT4u2bFvNiu8COnjap9ADD0dJ2hR0Fudhi0uOT7Qo72opvqAMtZiq2mHyqvcXUpoVXLUb99PAot5hc+4Z+GcotZqO9xTROCytv6JvtAjtaNU8L/7o/aJ9uyy3GZK+2C+yqdVpYCj3tLWaetE4L//D2he0KO5KmhWep9hZTrMUWY0aLZaDEsvFdC1oOJCpL9LRI4/frr+bh5Jv16ZJ4h9qH8yq2XVPfouzrgGzz3SuqT+IVTj/Cs7A6BVyIm0Gt4spP8SRuJrS6r8zi9+79DM+iykUmBxvNWXL4t8Kt+D2qVXWFQ+F4V6/qWzoQ5zJaVTvNYCEeoCmVfRlHrN3Li7B2J3QqnmMrlGT1PZQ8I1UnmVyfGz069HGa+EnTTDDIxClG4mvhZWbRsL6zlRH6TbIJhkoEwrHRXpjV5OpnpSXpYkK+1n5gUdoJI8Uw1D7SL8399o1Gkg1tl9eHQgr/TPv1iwsp/D03/h8jhf/ooP30oiSFf5Rqv0dTEsN/ov2M5kIK//Gz7ep6QfjbLq8P81wI/5jwV0IK/7ufEP7a7++VCH/b5fXiB4T/9qeHvxP9Rgx/34n9lBT+4d52df2YtYf/WPudxQ/t4R+p/3vXh/bwH9surS+t4T92op2WWsI/dWK3eNEc/g49w5bwT21X1auG8A9Ptovq13X4Z05soqpq4T9yJQ4rFtXwj4ztcm4hyD6T0XfjIONKcYzzMIpC35GRW5PV4rTfvO6c2P8CAAAAAAAAAAAAAAAAAAAAAAAAAPr0G6/bPLLO/+s1AAAAAElFTkSuQmCC" width="20" height="20"/></span>
                </button>) : (
                null
              )}
              Side {this.sideNr} av {this.antSider}
              {((this.artikkelNrStart+this.antallSakerPrSide)<this.antallArtikler) ? (
                <button type="button" style={{padding: 0, border: "none", backgroundColor: "white"}} className=" ml-4" onClick={this.tilNesteSide}>
                  <span><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRmwkkdG4LNdc5UHxV-9-bW89z6H4S_TyqStkyMEhVnrrov5p-M" width="20" height="20"/></span>
                </button>) : (
                null
              )}
            </div>
          </footer>
        </Oppsett>
      </div>
    );
  }

  tilNesteSide(){
    this.artikkelNrStart+= this.antallSakerPrSide;
    this.sideNr++;
    sakService
      .getNyhetssakerGittKategori(this.props.match.params.kategorinavn,this.artikkelNrStart)
      .then(kategorien => {
        this.delt.kategori = kategorien;
        this.endreRekkefolge();
      })
      .catch((error: Error) => Alert.danger(error.message));
    }

  endreRekkefolge() {
    let mellomlager = this.delt.kategori.filter((e, i) => i % 3 === 0);
    mellomlager = mellomlager.concat(this.delt.kategori.filter((e, i) => ((i+2) % 3) === 0));
    mellomlager = mellomlager.concat(this.delt.kategori.filter((e, i) => ((i+1) % 3) === 0));
    this.delt.kategori = mellomlager;
  }

  tilForrigeSide(){
    this.artikkelNrStart-= this.antallSakerPrSide;
    this.sideNr--;
    sakService
      .getNyhetssakerGittKategori(this.props.match.params.kategorinavn,this.artikkelNrStart)
      .then(kategorien => {
        this.delt.kategori = kategorien;
        this.endreRekkefolge();
      })
      .catch((error: Error) => Alert.danger(error.message));
   }

  mounted(){
    this.artikkelNrStart = 0;
    this.sideNr=1;
    sakService
      .getNyhetssakerGittKategori(this.props.match.params.kategorinavn,this.artikkelNrStart)
      .then(kategorien => {
        this.delt.kategori = kategorien;
        this.endreRekkefolge();
         })
      .catch((error: Error) => Alert.danger(error.message));

    sakService
      .getAntSakerKategori(this.props.match.params.kategorinavn)
      .then(antall => {
        this.antallArtikler = antall[0].antall;
        this.antSider = Math.ceil(this.antallArtikler/this.antallSakerPrSide);
        console.log(antall[0].antall);
      })
      .catch((error: Error) => Alert.danger(error.message));


  }
}

class Artikkelside extends Component<{match: { params: { artikkelID: number }}}>{
  delt ={kommentarer: [],antKommentarer: "kommentar",
    sak: {
      artikkelID: this.props.match.params.artikkelID,
      overskrift: "",
      bildelink: "",
      innhold: "",
      tidspunkt: "",
      antallLikesArtikkel: 0,
      kategoriNavn: "",
      viktighet: 1
    }};

  antKommentarerLastetInn = 5;
  alleKommentarer = [];
  aktivDropDownList = true;
  kommentar = new OpprettKommentarer();
  buttonColor = "btn btn-default btn-sm ml-2";
  sorteringsRekkefolge= "desc";
  sorterEtterKolonne="kommentarID";

  render(){
    if(!this.delt.sak) return null;
    return(
      <div>
        <Oppsett sideBredde={1} midtBredde={10}>
          <div>
          <br></br>
          <div>
            <div className="text-center">
            <Overskrift>{this.delt.sak.overskrift}</Overskrift>
            <br></br>
            <img src={this.delt.sak.bildelink} className="mx-1 d-block img-fluid card-img-top">
            </img>
            <p><i>Artikkelen ble opprettet: {this.delt.sak.tidspunkt}</i></p>
              <div>
                <p> {this.delt.sak.antallLikesArtikkel} {(this.delt.sak.antallLikesArtikkel === 1) ? ("like") : ("likes")} {'  '}
                <button type="button" className={this.buttonColor} onClick={this.skiftFarge}>
                  <span><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRA1BKCr5a1zU_9EFiRMzRzibk9awD3kGSwyS9kRqhzqAaYYvn_" width="40" height="40"/></span>
                </button>
                </p>
              </div>
              <br></br>
            </div>
          <Oppsett sideBredde={1} midtBredde={10}>
            <div dangerouslySetInnerHTML={{__html: Base64.decode(this.delt.sak.innhold)}}></div>
          </Oppsett>
        </div>
          <br></br>
          <br></br>
          <Oppsett sideBredde={2} midtBredde={8}>
            <div>
              <div className={"card-footer bg-secondary"}>
              <div>
                <br></br>
                <form id="kommentarFelt">
                  <Input tittelInput={"Navn"}>
                    <input
                      type="text"
                      placeholder="Skriv inn navn"
                      onChange={(event: SyntheticInputEvent<HTMLInputElement>) => { this.kommentar.kommentarNavn = event.target.value;}}
                    />
                  </Input>
                  <br/>
                  <textarea className="form-control" placeholder="Tekst" rows="3"  id="comment" name="text"
                            onChange={(event: SyntheticInputEvent<HTMLInputElement>) => { this.kommentar.innhold = event.target.value;}}
                  />
                  <br/>
                  <button type="button" onClick={this.leggTilKommentar}>
                    Legg til kommentar
                  </button>
                  <br/>
                  <p>
                    {(this.alleKommentarer.length>0) ? (this.alleKommentarer.length) : ('')} {this.delt.antKommentarer}
                    <select
                        onChange={this.sorter}
                        className="form-control float-right"
                        disabled={this.aktivDropDownList}
                        style={{width: 180}}
                        placeholder="Select a person">
                      <option hidden> Sorter etter</option>
                      <option value={"Eldste"} key="Eldste"> Eldste</option>
                      <option value={"Nyeste"} key="Nyeste"> Nyeste</option>
                      <option value={"Mest populære"} key="Mest Populære"> Mest Populære</option>
                    </select>
                  </p>
                </form>
              </div>
              <br></br>
              <div >
                  {this.delt.kommentarer.map(kommentar => (
                    <ListGroup.Item key={kommentar.kommentarID}>
                      <b>{kommentar.kommentarNavn} {'  '}<small>{'Publisert:'} {kommentar.tidspunkt}</small></b>
                      <br></br>
                      {kommentar.innhold}
                      <p className="float-right">

                      </p>
                      <br></br>
                      <a href={"/#/nyheter/"+this.delt.sak.kategoriNavn+"/"+this.delt.sak.artikkelID} onClick={() => {this.likerKommentar(kommentar.kommentarID)}}>Liker</a>
                      <span>
                        <img src={"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRA1BKCr5a1zU_9EFiRMzRzibk9awD3kGSwyS9kRqhzqAaYYvn_"} height={30} width={30}/>
                      </span> {(kommentar.antallLikesKommentar>0) ? (kommentar.antallLikesKommentar) : ('')}
                    </ListGroup.Item>
                  ))}
              </div>
              <br></br>
                {(this.antKommentarerLastetInn<this.alleKommentarer.length) ? (<div className={"text-center"}>
                  <button type={"button"} onClick={this.lastInnKommentarer}>
                    Last inn flere kommentarer
                  </button>
                </div>) : (
                  null
                )}
              <br></br>
            </div>
            </div>
          </Oppsett>
          </div>
        </Oppsett>
      </div>
    )
  }

  lastInnKommentarer(){
    this.antKommentarerLastetInn+= 5;
    if(this.antKommentarerLastetInn>this.alleKommentarer.length){
      this.antKommentarerLastetInn = this.alleKommentarer.length;
    }
    this.delt.kommentarer = this.alleKommentarer.slice(0,this.antKommentarerLastetInn);
  }

  likerKommentar(kommentarID) {
    let denneKommentar = this.delt.kommentarer.find(kommentar => (kommentar.kommentarID === kommentarID));
    console.log(denneKommentar);
    if(denneKommentar) {
      sakService
        .oppdaterLikesKommentar(this.props.match.params.artikkelID, kommentarID, (denneKommentar.antallLikesKommentar + 1))
        .then(() => {
          sakService
            .getKommentarerGittSak(this.props.match.params.artikkelID, this.sorterEtterKolonne, this.sorteringsRekkefolge)
            .then(kommentarer => {
              this.alleKommentarer = kommentarer;
              this.delt.kommentarer = kommentarer.slice(0,this.antKommentarerLastetInn);
            })
            .catch((error: Error) => Alert.danger(error.message));
        })
        .catch((error: Error) => Alert.danger(error.message));
    }
  }

  sorter(e){
    let {value} = e.target;
    console.log(value);
    if(value === "Eldste"){
      this.sorteringsRekkefolge = "asc";
      this.sorterEtterKolonne = "kommentarID";
      console.log("Eldste");
    } else if(value === "Nyeste"){
      console.log("Nyeste");
      this.sorteringsRekkefolge = "desc";
      this.sorterEtterKolonne = "kommentarID";
    } else{
      console.log("Populær");
      this.sorteringsRekkefolge = "desc";
      this.sorterEtterKolonne = "antallLikesKommentar";
    }
    console.log(this.props.match.params.artikkelID,this.sorterEtterKolonne,this.sorteringsRekkefolge);
    sakService
      .getKommentarerGittSak(this.props.match.params.artikkelID,this.sorterEtterKolonne,this.sorteringsRekkefolge)
      .then(kommentarer => {
        this.delt.kommentarer = kommentarer;
        if(this.delt.kommentarer.length>1){
          this.delt.antKommentarer="kommentarer";
        }
        if(kommentarer.length>this.antKommentarerLastetInn){
          this.delt.kommentarer = kommentarer.slice(0,this.antKommentarerLastetInn);
          this.alleKommentarer = kommentarer;
        }
      })
      .catch(() => {this.delt.antKommentarer="Ingen kommentarer";});
  }

  skiftFarge(){
    if(this.buttonColor === "btn btn-default btn-sm ml-2"){
      if(this.delt.sak.antallLikesArtikkel === null){
        this.delt.sak.antallLikesArtikkel = 0;
      }
      sakService.oppdaterLikesArtikkel(this.props.match.params.artikkelID,(this.delt.sak.antallLikesArtikkel+1))
        .then(() => {
          this.buttonColor = "btn btn-success btn-sm ml-2";
          sakService
            .getArtikkelID(this.props.match.params.artikkelID)
            .then(artikkel => (this.delt.sak = artikkel[0]))
            .catch((error: Error) => Alert.danger(error.message));
        })
        .catch((error: Error) => Alert.danger(error.message));
    } else {
      sakService.oppdaterLikesArtikkel(this.props.match.params.artikkelID,(this.delt.sak.antallLikesArtikkel-1))
        .then(() => {
          this.buttonColor = "btn btn-default btn-sm ml-2";
          sakService
            .getArtikkelID(this.props.match.params.artikkelID)
            .then(artikkel => (this.delt.sak = artikkel[0]))
            .catch((error: Error) => Alert.danger(error.message));
        })
        .catch((error: Error) => Alert.danger(error.message))

    }
  }

  leggTilKommentar(){
    this.kommentar.nyhetssakID = this.props.match.params.artikkelID;
    sakService
      .leggTilKommentar(this.kommentar)
      .then(() => {
        this.aktivDropDownList = false;
        sakService
          .getKommentarerGittSak(this.props.match.params.artikkelID,this.sorterEtterKolonne,this.sorteringsRekkefolge)
          .then(kommentarer => {
            this.delt.kommentarer = kommentarer;
            this.alleKommentarer = kommentarer;
            // $FlowFixMe
            document.getElementById("kommentarFelt").reset();
            if(kommentarer.length>1){
              this.delt.antKommentarer="kommentarer";
            } else if(kommentarer.length==1){
              this.delt.antKommentarer="kommentar";
            }
            if(kommentarer.length>this.antKommentarerLastetInn){
              this.delt.kommentarer = kommentarer.slice(0,this.antKommentarerLastetInn);
            }
            this.aktivDropDownList = false;
          })
          .catch(() => {
            this.delt.antKommentarer="Ingen kommentarer";
          });
      })
      .catch((error: Error) => Alert.danger(error.message));
    console.log("Alt bra");

  }

  mounted(){
    sakService
      .getArtikkelID(this.props.match.params.artikkelID)
      .then(artikkel => (this.delt.sak = artikkel[0]))
      .catch((error: Error) => Alert.danger(error.message));

    sakService
      .getKommentarerGittSak(this.props.match.params.artikkelID,this.sorterEtterKolonne,this.sorteringsRekkefolge)
      .then(kommentarer => {
        this.delt.kommentarer = kommentarer;
        this.alleKommentarer = kommentarer;
        if(this.delt.kommentarer.length>1){
          this.delt.antKommentarer="kommentarer";
        }
        if(kommentarer.length>this.antKommentarerLastetInn){
          this.delt.kommentarer = kommentarer.slice(0,this.antKommentarerLastetInn);
        }
        this.aktivDropDownList = false;
      })
      .catch(() => {this.delt.antKommentarer="Ingen kommentarer";});
  }
}

class EndreArtikkel extends Component{
  alleNyhetssaker = [];

  checkBox = {
    sport: {gyldighet: false, navn: "Sport", disable: false},
    kultur: {gyldighet: false, navn: "Kultur", disable: false},
    tech: {gyldighet: false, navn: "Tech", disable: false}
  };

  aktivCheckBox = "";

  delt = {nyhetssaker: []};

  sok = {
    innhold: ''
  };

  handterInput(e) {
    this.sok.innhold = e.target.value;
    console.log(this.sok.innhold);
    if (this.sok.innhold.length >0) {
      if(this.checkBox.kultur.disable ||
        this.checkBox.sport.disable ||
        this.checkBox.tech.disable){
        sakService
          .filtrerPaaKategoriOgOverskrift(this.aktivCheckBox,this.sok.innhold)
          .then(sak => (this.delt.nyhetssaker = sak))
          .catch();
      } else {
        sakService
          .filtrerArtikler(this.sok.innhold)
          .then(sak => (this.delt.nyhetssaker = sak))
          .catch();
      }
    } else {
      if(this.checkBox.kultur.disable ||
        this.checkBox.sport.disable ||
        this.checkBox.tech.disable){
        this.delt.nyhetssaker = this.alleNyhetssaker.filter(nyhet => (nyhet.kategoriNavn === this.aktivCheckBox));
      } else {
        sakService
          .getAlleArtikler()
          .then(nyeste => (this.delt.nyhetssaker = nyeste))
          .catch();
      }
    }
  }

  render() {
    return(
      <Oppsett sideBredde={2} midtBredde={8}>
        <Overskrift>Administrer nyhetssaker</Overskrift>
        <div>

          <br></br>

        </div>
        <br></br>
          <Input tittelInput="Søk">
            <input
              className={"form-control mr-5"}
              type="text"
              value={this.sok.innhold}
              placeholder="Søk etter nyhetssak"
              onChange = {this.handterInput}
            />
            <NavLink activeStyle={{ color: 'darkblue' }} to={"/registrerNyhetssak"}>
              <button className="float-right btn btn-success" >Lag ny artikkel</button>
            </NavLink>
          </Input>
        <ContainerFluid>
          <Row>
            <Column bredde={4}>
              <p>Filtre:</p>
            </Column>
            <Column bredde={8}>
              <CheckBox checkBoxNavn={"Sport"} disable={this.checkBox.sport.disable} forandring={() => this.checkSport()}/>
              <CheckBox checkBoxNavn={"Kultur"} disable={this.checkBox.kultur.disable} forandring={() => this.checkKultur()}/>
              <CheckBox checkBoxNavn={"Tech"} disable={this.checkBox.tech.disable} forandring={() => this.checkTech()}/>
            </Column>
          </Row>
        </ContainerFluid>
        <ul className="list-group ">
          {this.delt.nyhetssaker.map(sak => (
            <ListGroup.Item key={sak.artikkelID}>
              <NavLink activeStyle={{ color: 'darkblue' }} style={{color: "black"}} to={"/nyheter/"+sak.kategoriNavn+"/"+sak.artikkelID}>
              {sak.overskrift}
              </NavLink>
              <Popup trigger={<button className="float-right btn btn-danger"> Slett</button>} position="right center">
                { close => (
                  <div>
                  <p><b>Bekreft at du vil slette denne artikkelen?</b></p>
                  <button className="float-right btn btn-secondary mr-2" onClick={() => {close();}}>Avbryt</button>
                  <button className="btn btn-danger float-right mr-3" onClick={() => {this.slett(sak.artikkelID)}}>Slett</button>
                </div>
                )}
              </Popup>
              <NavLink activeStyle={{ color: 'darkblue' }} to={"/oppdater/"+sak.artikkelID}>
                <button className="float-right btn btn-secondary mr-2" >Oppdater</button>
              </NavLink>
            </ListGroup.Item>
          ))}
        </ul>
      </Oppsett>
    )
  }

  checkSport(){
    this.checkBox.sport.gyldighet = !this.checkBox.sport.gyldighet;
    if(this.checkBox.sport.gyldighet){
      this.aktivCheckBox = this.checkBox.sport.navn;
      this.delt.nyhetssaker = this.delt.nyhetssaker.filter(nyhet => (nyhet.kategoriNavn === this.checkBox.sport.navn));
      this.checkBox.kultur.disable=true;
      this.checkBox.tech.disable=true;
    } else {
      this.delt.nyhetssaker = this.alleNyhetssaker;
      this.checkBox.kultur.disable=false;
      this.checkBox.tech.disable=false;
    }
  }

  checkKultur(){
    this.checkBox.kultur.gyldighet = !this.checkBox.kultur.gyldighet;
    if(this.checkBox.kultur.gyldighet){
      this.aktivCheckBox = this.checkBox.kultur.navn;
      this.delt.nyhetssaker = this.delt.nyhetssaker.filter(nyhet => (nyhet.kategoriNavn === this.checkBox.kultur.navn));
      this.checkBox.sport.disable=true;
      this.checkBox.tech.disable=true;
    } else {
      this.delt.nyhetssaker = this.alleNyhetssaker;
      this.checkBox.sport.disable=false;
      this.checkBox.tech.disable=false;
    }
  }

  checkTech(){
    this.checkBox.tech.gyldighet = !this.checkBox.tech.gyldighet;
    if(this.checkBox.tech.gyldighet){
      this.aktivCheckBox = this.checkBox.tech.navn;
      this.delt.nyhetssaker = this.delt.nyhetssaker.filter(nyhet => (nyhet.kategoriNavn === this.checkBox.tech.navn));
      this.checkBox.sport.disable=true;
      this.checkBox.kultur.disable=true;
    } else {
      this.delt.nyhetssaker = this.alleNyhetssaker;
      this.checkBox.sport.disable=false;
      this.checkBox.kultur.disable=false;
    }
  }

  slett(artikkelID) {
    console.log(artikkelID);
    sakService
      .slettNyhetssak(artikkelID)
      .then( () => {
        this.mounted();
        this.sok.innhold = '';
      })
      .catch((error: Error) => Alert.danger(error.message));
  }


  mounted(){
    sakService
      .getAlleArtikler()
      .then(nyeste => {
        this.delt.nyhetssaker = nyeste;
        this.alleNyhetssaker = nyeste;
      })
      .catch((error: Error) => Alert.danger(error.message));
  }
}

class OppdaterArtikkel extends Component<{match: { params: {artikkelID: number}}}>{

  artikkel = new Sak();

  form = null;

  kategoriListe = [];

  delt = sharedComponentData({kategoriListe: []});

  viktighetListe(e) {
    let {value} = e.target;
    console.log(value);
    this.artikkel.viktighet = value;
  }

  kategoriListe(e){
    let {value} = e.target;
    console.log(value);
    this.artikkel.kategoriNavn = value;
  }


  render(){
    if(!this.artikkel) return null;
    return(
      <div>
        <Oppsett midtBredde={8} sideBredde={2}>
          <Overskrift>Oppdater nyhetssak</Overskrift>
          <br></br>
          <br></br>
          <p className="text-center"><img src={this.artikkel.bildelink} width="300" height="200"/></p>
          <p className="font-italic text-center">Se nåværende bilde over</p>
          <form ref={e => (this.form = e)}>
            <Input tittelInput="Overskrift">
              <input type="text" className="form-control"
                     value={this.artikkel.overskrift}
                     onChange={(event: SyntheticInputEvent<HTMLInputElement>) => (this.artikkel.overskrift = event.target.value)}/>
            </Input>
            <Input tittelInput="Bildelink">
              <input type="text" className="form-control"
                     value={this.artikkel.bildelink}
                     onChange={(event: SyntheticInputEvent<HTMLInputElement>) => (this.artikkel.bildelink = event.target.value)}/>
            </Input>
            <ContainerFluid >
              <Row>
                <Column bredde={6}>
                  <label> Viktighet:</label>
                  <br></br>
                  <select
                    onChange={this.viktighetListe}
                    className="form-control float-left"
                    style={{width: 200}}
                    value={this.artikkel.viktighet}>
                    <option value="1" key="1"> Viktighet 1</option>
                    <option value="2" key="2"> Viktighet 2</option>
                  </select>
                </Column>
                <Column bredde={6}>
                  <label> Kategori:</label>
                  <br></br>
                  <select
                    onChange={this.kategoriListe}
                    className="form-control"
                    style={{width: 200}}
                    value={this.artikkel.kategoriNavn}>
                    {this.kategoriListe.map(e => (
                      <option value={e.navn} key={e.navn}> {e.navn}</option>))
                    }
                  </select>
                </Column>
              </Row>
            </ContainerFluid>
            <br></br>
            <div className="form-group">
            <textarea className="form-control" placeholder="Innhold..." rows="6"  id="comment" name="text"
                      value={this.artikkel.innhold}
                      onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {this.artikkel.innhold = event.target.value;}}
            />
            </div>
          </form>
          <div className="text-center">
            <button type="button" className="btn btn-ghost btn-ghost-bordered center-block" onClick={this.oppdater}> Oppdater </button>
          </div>
          <br></br>
          <br></br>
        </Oppsett>
      </div>
    )
  }

  oppdater () {
     const oppdaterSak = {
      overskrift: this.artikkel.overskrift,
      bildelink: this.artikkel.bildelink,
      innhold: this.artikkel.innhold,
      kategoriNavn: this.artikkel.kategoriNavn,
      viktighet: this.artikkel.viktighet
    };
     console.log(oppdaterSak);
    sakService
      .oppdaterArtikkel(this.props.match.params.artikkelID,this.artikkel)
      .then(() => {
        history.push("/nyheter/"+this.artikkel.kategoriNavn+"/"+this.artikkel.artikkelID);
        Alert.success("Du har oppdatert "+this.artikkel.overskrift);
      })
      .catch((error: Error) => Alert.danger(error.message));
  }

  mounted(){
    sakService
      .getArtikkelID(this.props.match.params.artikkelID)
      .then(nyhet => {
        this.artikkel = nyhet[0];
        this.artikkel.innhold = Base64.decode(this.artikkel.innhold);
      })
      .catch((error: Error) => Alert.danger(error.message));

    kategoriService
      .getKategorier()
      .then(kategori => {
        this.kategoriListe = kategori;
      })
      .catch((error: Error) => Alert.danger(error.message));
  }
}

class RegistrerArtikkel extends Component{
  alleKategorier = [];
  viktighet = 1;
  kategori = "";
  form = null;

  opprettSak = new OpprettSak();
  render(){
    return(
      <Oppsett midtBredde={8} sideBredde={2}>
        <Overskrift>Registrer artikkel</Overskrift>
        <br></br>
        <br></br>

        <form ref={e => (this.form = e)}>
          <ContainerFluid>
            <Row>
              <Column bredde={6}>
                <Input tittelInput="Overskrift">
                  <input type="text" className="form-control" placeholder="Skriv inn overskrift"
                         required={true}
                        onChange={(event: SyntheticInputEvent<HTMLInputElement>) => (this.opprettSak.overskrift = event.target.value)}/>
                </Input>
                <Input tittelInput="Bildelink  ">
                  <input type="text" className="form-control" placeholder="Lim inn bildelink her"
                         required={true}
                         onChange={(event: SyntheticInputEvent<HTMLInputElement>) => (this.opprettSak.bildelink = event.target.value)}/>
                </Input>
                <ContainerFluid>
                  <Row>
                    <Column bredde={6}>
                      <br/>
                      <br></br>
                      <select
                          onChange={this.viktighetListe}
                          className="form-control float-left "
                          style={{width: 180}}
                      >
                        <option hidden> Velg viktighet</option>
                        <option value={1} key="1"> Viktighet 1</option>
                        <option value={2} key="2"> Viktighet 2</option>
                      </select>
                    </Column>
                    <Column bredde={6}>
                      <br/>
                      <br></br>
                      <select
                          onChange={this.kategoriListe}
                          className="form-control "
                          style={{width: 180}}
                      >
                        <option hidden> Velg kategori</option>
                        {this.alleKategorier.map(e => (
                            <option value={e.navn} key={e.navn}> {e.navn}</option>))
                        }
                      </select>
                    </Column>
                  </Row>
                </ContainerFluid>


              </Column>
              <Column bredde={6}>
                <p className="text-center"><img src={this.opprettSak.bildelink} width="300" height="200"/></p>
              </Column>
            </Row>
            <Row>
              <Column bredde={6}>

              </Column>
            </Row>

          </ContainerFluid>
          <ContainerFluid>
            <Row>
              <Column bredde={6}>

              </Column>
              <Column bredde={6}>

              </Column>
            </Row>
          </ContainerFluid>
          <br></br>
          <div className="form-group">
            <textarea className="form-control" placeholder="Innhold..." rows="6"  id="comment" name="text"
                      required={true}
                      onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {this.opprettSak.innhold = event.target.value;}}
            />
          </div>
        </form>
        <div className="text-center">
          <button type="button" className="btn btn-ghost btn-ghost-bordered center-block" onClick={this.registrer}> Registrer </button>
        </div>
        <br></br>
        <br></br>
      </Oppsett>
    )
  }

  registrer(){
    if(!this.form || !this.form.checkValidity()){
      return Alert.danger("Fyll ut alle feltene");
    }
    sakService
      .opprettArtikkel(this.opprettSak)
      .then(() => {Alert.success("Du har registrert en ny artikkel!")})
      .catch((error: Error) => Alert.danger(error.message));
    history.push('/nyheter/'+this.opprettSak.kategoriNavn);
  }

  viktighetListe(e) {
    let {value} = e.target;
    console.log(value);
    this.opprettSak.viktighet = value;
  }

  kategoriListe(e){
    let {value} = e.target;
    console.log(value);
    this.opprettSak.kategoriNavn = value;
  }

  mounted(){
    kategoriService
      .getKategorier()
      .then(kategorien => (this.alleKategorier = kategorien))
      .catch((error: Error) => Alert.danger(error.message));
  }
}

const root = document.getElementById('root');
if (root)
  ReactDOM.render(
    <HashRouter>
      <div>
        <Alert/>
        <Menu/>
        <Switch>
          <Route exact path="/" component={Redirection} />
          <Route exact path="/#/" component={Redirection} />
          <Route exact path="/nyheter" component={ForsideVisning} />
          <Route exact path="/nyheter/:kategorinavn" component={Kategori} />
          <Route exact path="/nyheter/:kategorinavn/:artikkelID" component={Artikkelside}/>
          <Route path="/registrer" component={EndreArtikkel}/>
          <Route exact path="/oppdater/:artikkelID" component={OppdaterArtikkel}/>
          <Route path="/registrerNyhetssak" component={RegistrerArtikkel}/>
        </Switch>
        <Footer/>
      </div>
    </HashRouter>,
    root
  );