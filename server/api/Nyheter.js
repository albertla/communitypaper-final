// @flow
const express = require('express');
const router = express.Router();
var mysql = require("mysql");
var bodyParser = require("body-parser");
const ArtikkelDao = require("../dao/ArtikkelDao.js");

type Request = express$Request;
type Response = express$Response;


var pool = mysql.createPool({
    connectionLimit: 5,
    host: "mysql.stud.iie.ntnu.no",
    user: "albertla",
    password: "oIZSwarR",
    database: "albertla",
    debug: false,
    multipleStatements: true
});

let artikkelDao = new ArtikkelDao(pool);

// gjort
router.post("/opprettKommentar/sak", (req : Request, res: Response) => {
  console.log("Fikk POST-request fra klienten");
  artikkelDao.lagKommentar(req.body, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

//gjort
router.put("/likes/:artikkelID", (req : Request, res: Response) => {
  console.log(" /nyheter/likes/:artikkelID fikk PUT-request fra klienten");
  artikkelDao.oppdaterLikesArtikkel(req.body, req.params.artikkelID, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

//gjort
router.put("/likes/:sakID/:kommentarID", (req : Request, res: Response) => {
  console.log(" /nyheter/likes/:sakID/:kommentarID fikk PUT-request fra klienten");
  artikkelDao.oppdaterLikesKommentar(req.body, req.params.kommentarID, (status, data) => {
    res.status(status);
    res.json(data);
  });
});
//gjort
router.get("/kommentarer/:sakID/:sorterEtterKolonne/:sorteringsRekkefolge",(req : Request, res: Response) => {
  console.log("/nyheter/kommentarer/:sakID/:sorteringsrekkefolge fikk request fra klient");
  artikkelDao.getKommentarerArtikkel(req.params.sakID,req.params.sorterEtterKolonne,req.params.sorteringsRekkefolge,(status,data) => {
    res.status(status);
    res.json(data);
  });
});

//gjort
router.get("/side/:sakNrStart",(req : Request, res: Response) => {
    console.log("/nyheter/side/sakNrStart: fikk request fra klient");
    artikkelDao.getArtikkelViktighet1(req.params.sakNrStart,(status,data) => {
        res.status(status);
        res.json(data);
    });
});
//Gjort
router.get("/antallSaker",(req : Request, res: Response) => {
  console.log("/antallSaker: fikk request fra klient");
  artikkelDao.getAntArtiklerForside((status,data) => {
    res.status(status);
    res.json(data);
  });
});
//gjort
router.get("/antallSaker/:kategoriNavn",(req : Request, res: Response) => {
  console.log("/antallSaker: fikk request fra klient");
  artikkelDao.getAntArtiklerKategori(req.params.kategoriNavn,(status,data) => {
    res.status(status);
    res.json(data);
  });
});

//gjort
router.get("/gittKategori/:kategori/:sakNrStart", (req : Request, res: Response) => {
  console.log("/kategori/:kategori: fikk request fra klient");
  console.log(req.params.kategori);
  artikkelDao.getArtiklerGittKategori(req.params.sakNrStart,req.params.kategori, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

router.get("/kategori/:artikkelID", (req : Request, res: Response) => {
    console.log("/kategori/:artikkelID: fikk request fra klient");
    console.log(req.params);
    artikkelDao.getArtikkelID(req.params.artikkelID, (status, data) => {
        res.status(status);
        res.json(data);
    });
});
//gjort
router.get("/kategorier", (req : Request, res: Response) => {
  console.log("/kategori fikk request fra klient");
  artikkelDao.getKategorier((status,data) => {
    res.status(status);
    res.json(data);
  })
});

router.get("/liveFeed",(req : Request, res: Response) => {
  console.log("/liveFeed/:nr fikk request fra klient");
  console.log(req.params.nr);
  artikkelDao.getLiveFeed((status,data) => {
    res.status(status);
    res.json(data);
  })
});

module.exports = router;