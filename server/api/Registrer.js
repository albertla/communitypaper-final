const express = require('express');
const router = express.Router();
var mysql = require("mysql");
var bodyParser = require("body-parser");
var Base64 = require('js-base64').Base64;

type Request = express$Request;
type Response = express$Response;

const ArtikkelDao = require("../dao/ArtikkelDao.js");

var pool = mysql.createPool({
    connectionLimit: 5,
    host: "mysql.stud.iie.ntnu.no",
    user: "albertla",
    password: "oIZSwarR",
    database: "albertla",
    debug: false,
    multipleStatements: true
});

let artikkelDao = new ArtikkelDao(pool);

//gjort
router.get("/",(req : Request, res: Response) => {
  console.log("/registrer/ fikk request fra klient");
  artikkelDao.getAlleArtikler((status,data) => {
    res.status(status);
    res.json(data);
  });
});

//
router.get("/filtrer/:kategoriNavn/:sokeOrd",(req : Request, res: Response) => {
  console.log("/registrer/ fikk request fra klient");
  artikkelDao.filtrerArtiklerGittKategoriOgOverskrift(req.params.kategoriNavn,req.params.sokeOrd,(status,data) => {
    res.status(status);
    res.json(data);
  });
});

//
router.post("/", (req: Request,res: Response) => {
    console.log("Fikk POST-request fra klienten");
    req.body.innhold = Base64.encode(req.body.innhold);
    artikkelDao.opprettArtikkel(req.body, (status, data) => {
        res.status(status);
        res.json(data);
    });
});

//
router.put("/:artikkelID", (req: Request,res: Response) => {
    console.log("Fikk put request fra klienten");
    req.body.innhold = Base64.encode(req.body.innhold);
    artikkelDao.oppdaterArtikkel(req.body, req.params.artikkelID, (status, data) => {
        res.status(status);
        res.json(data);
    });
});

//
router.delete("/:sakID", (req: Request,res: Response) => {
    console.log("Fikk slettet en sak");
    artikkelDao.slettArtikkel(req.params.sakID, (status, data) => {
        res.status(status);
        res.json(data);
    });
});

router.get("/filtrer/:sok", (req: Request,res: Response) => {
  console.log("/filtrer/:sok fikk get request fra klienten");
  artikkelDao.filtrerArtikler(req.params.sok, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

module.exports = router;