// @flow
var mysql = require("mysql");

const ArtikkelDao = require("../dao/ArtikkelDao.js");
const runsqlfile = require("../dao/runsqlfile.js");
const Dao = require("../dao/dao.js");



/*
var pool = mysql.createPool({
    connectionLimit: 1,
    host: "mysql",
    user: "root",
    password: "root",
    database: "School",
    debug: false,
    multipleStatements: true
});*/

// GitLab CI Pool
var pool = mysql.createPool({
  connectionLimit: 1,
  host: "mysql.stud.iie.ntnu.no",
  user: "albertla",
  password: "oIZSwarR",
  database: "albertla",
  debug: false,
  multipleStatements: true
});

let artikkelDao = new ArtikkelDao(pool);
//let ntnuNyhetssakDao = new NyhetssakDao(ntnuPool);

beforeAll(done => {
    runsqlfile("dao/createTable.sql", pool, () => {
        runsqlfile("dao/createTestdata.sql",pool,done);
    });
});

afterAll(() => {
    pool.end();
});

test("Få artikkelid fra db", done => {
    function callback(status, data){
        console.log(
            "Test callback: status " + status + ", data= "+ JSON.stringify(data)
        );
        expect(data.length).toBe(1);
        expect(data[0].overskrift).toBe("Ole Gunnar Solskjær får 20 kr");
        done();
    }
    artikkelDao.getArtikkelID(3, callback);
});


test("Opprett nyhetssak", done => {
    function callback(status, data){
        console.log(
            "Test callback: status " + status + ", data= "+ JSON.stringify(data)
        );
        expect(data.affectedRows).toBeGreaterThanOrEqual(1);
        done();
    }

    artikkelDao.opprettArtikkel(
        {overskrift: "Sandefjord Fotball rykker opp", innhold: "Men de kommer til å rykke ned neste år", bildelink: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRtksGdWoGlpbvkIMJkejQViEqJmSbllcKsLKcZxxt_7z0v5Lbc", viktighet: 2, kategoriNavn: "Sport"},
        callback
    );
});

test("oppdater en nyhetssak", done => {
    function callback(status, data){
        console.log(
            "Test callback: status " + status + ", data= "+ JSON.stringify(data)
        );
        expect(data.affectedRows).toBeGreaterThanOrEqual(1);
        done();
    }

    artikkelDao.oppdaterArtikkel(
        {overskrift: "Arsenal på førsteplass", innhold: "Aprilsnarr på forskudd", bildelink: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSuH6IevrT_5Wg1-T60x_bsJ68PWoywKpjmWo0fxUlUMcqPKSGT", viktighet: 2, kategoriNavn: "Sport"},
        3,
        callback
    );
});

test("Oppdater likes", done => {
  function callback(status, data){
    console.log(
      "Test callback: status " + status + ", data= "+ JSON.stringify(data)
    );
    expect(data.affectedRows).toBeGreaterThanOrEqual(1);
    done();
  }

  artikkelDao.oppdaterLikesArtikkel({antallLikes: 3},4,
    callback
  );
});

test("Slett én nyhetssak", done => {
    function callback(status, data){
        console.log(
            "Test callback: status " + status + ", data= "+ JSON.stringify(data)
        );
        expect(data.affectedRows).toBeGreaterThanOrEqual(1);
        done();
    }

    artikkelDao.slettArtikkel(
        5,
        callback
    );
});

test("Hent nyhetssaker med viktighet 1", done => {
  function callback(status, data){
    console.log(
      "Test callback: status " + status + ", data= "+ JSON.stringify(data)
    );
    expect(data.length).toBe(2);
    expect(data[0].overskrift).toBe("Bitcoin vil være verdt 50 øre");
    expect(data[1].overskrift).toBe("United taper igjen");
    done();
  }

  artikkelDao.getArtikkelViktighet1(0,
    callback
  );
});

test("Hent alle nyhetssakene", done => {
  function callback(status, data){
    console.log(
      "Test callback: status " + status + ", data= "+ JSON.stringify(data)
    );
    expect(data.length).toBe(5);
    expect(data[1].overskrift).toBe("Bitcoin vil være verdt 50 øre");
    expect(data[2].overskrift).toBe("Sandefjord Fotball rykker opp");
    expect(data[3].overskrift).toBe("Trump: \"Jeg har skapt mest trøbbel\"");
    done();
  }

  artikkelDao.getAlleArtikler(
    callback
  );
});


test("Hent alle nyhetssaker i en kategori", done => {
  function callback(status, data){
    console.log(
      "Test callback: status " + status + ", data= "+ JSON.stringify(data)
    );
    expect(data.length).toBe(2);
    expect(data[0].overskrift).toBe("Trump: \"Jeg har skapt mest trøbbel\"");
    expect(data[1].overskrift).toBe("Bitcoin vil være verdt 50 øre");
    done();
  }

  artikkelDao.getArtiklerGittKategori(0,"Tech",
    callback
  );
});

test("Hent alle kategoriene", done => {
  function callback(status, data){
    console.log(
      "Test callback: status " + status + ", data= "+ JSON.stringify(data)
    );
    expect(data.length).toBe(3);
    expect(data[0].navn).toBe("Kultur");
    expect(data[1].navn).toBe("Sport");
    expect(data[2].navn).toBe("Tech");
    done();
  }

  artikkelDao.getKategorier(
    callback
  );
});

test("Lag én kommentar", done => {
  function callback(status, data){
    console.log(
      "Test callback: status " + status + ", data= "+ JSON.stringify(data)
    );
    expect(data.affectedRows).toBe(1);
    done();
  }

  artikkelDao.lagKommentar({kommentarNavn: "Ferguson",innhold: "Disgrace",nyhetssakID: 1},
    callback
  );
});

test("Få kommentarene ut fra artikkelID", done => {
  function callback(status, data){
    console.log(
      "Test callback: status " + status + ", data= "+ JSON.stringify(data)
    );
    expect(data.length).toBe(2);
    expect(data[1].kommentarNavn).toBe("Ferguson");
    done();
  }

  artikkelDao.getKommentarerArtikkel(1,"kommentarID","asc",
    callback
  );
});
/*
test("Søk etter nyhetssaker", done => {
  function callback(status, data){
    console.log(
      "Test callback: status " + status + ", data= "+ JSON.stringify(data)
    );
    expect(data.length).toBe(1);
    expect(data[0].overskrift).toBe("Sandefjord rykker opp");
    done();
  }

  artikkelDao.filtrerNyhetssaker("Sandefjord rykker opp",
    callback
  );
});*/

test("Hent livefeeden", done => {
  function callback(status, data){
    console.log(
      "Test callback: status " + status + ", data= "+ JSON.stringify(data)
    );
    expect(data.length).toBe(5);
    expect(data[0].overskrift).toBe("Sandefjord Fotball rykker opp");
    expect(data[1].overskrift).toBe("Trump: \"Jeg har skapt mest trøbbel\"");
    expect(data[2].overskrift).toBe("Arsenal på førsteplass");
    expect(data[3].overskrift).toBe("Bitcoin vil være verdt 50 øre");
    done();
  }

  artikkelDao.getLiveFeed(
    callback
  );
});

test("Hent antall saker på framsiden", done => {
  function callback(status, data){
    console.log(
      "Test callback: status " + status + ", data= "+ JSON.stringify(data)
    );
    expect(data.length).toBe(1);
    expect(data[0].antall).toBe(2);
    done();
  }
  artikkelDao.getAntArtiklerForside(
    callback
  );
});

test("Hent antall saker på en gitt kategori", done => {
  function callback(status, data){
    console.log(
      "Test callback: status " + status + ", data= "+ JSON.stringify(data)
    );
    expect(data.length).toBe(1);
    expect(data[0].antall).toBe(2);
    done();
  }

  artikkelDao.getAntArtiklerKategori("Tech",
    callback
  );
});
/*
test("Filtrer på kategori og overskrift", done => {
  function callback(status, data){
    console.log(
      "Test callback: status " + status + ", data= "+ JSON.stringify(data)
    );
    expect(data.length).toBe(1);
    expect(data[0].overskrift).toBe("Sandefjord rykker opp");
    done();
  }

  filtrerArtiklerGittKategoriOverskrift("Sport","Sandefjord",
    callback
  );
});*/

test("Oppdater likes på én kommentar", done => {
  function callback(status, data){
    console.log(
      "Test callback: status " + status + ", data= "+ JSON.stringify(data)
    );
    expect(data.affectedRows).toBeGreaterThanOrEqual(1);
    done();
  }

  artikkelDao.oppdaterLikesKommentar({antallLikesKommentar: 4},1,
    callback
  )}
);
//});
/*
*/
/*

//let dao2 = new Dao(pool2);
*/
let dao = new Dao(pool);
test("sql error", done => {
  async function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    await expect(status).toBe(500);
    done();
  }

  dao.query("SELECT *", [], callback);
});

test("error connecting", done => {
  async function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    await expect(status).toBe(500);
    done();
  }

  //dao2.query("SELECT * FROM events", [], callback);
});

















