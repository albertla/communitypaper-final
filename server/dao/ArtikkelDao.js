// @flow

const Dao = require("./dao.js");

module.exports = class ArtikkelDao extends Dao {
  getArtikkelViktighet1(sakNrStart: number,callback: (status: string, data: string) => void){
      super.query(
          `SELECT artikkelID,overskrift,antallLikesArtikkel,bildelink,kategoriNavn FROM artikkel WHERE viktighet=1 ORDER BY antallLikesArtikkel DESC,artikkelID DESC LIMIT ${sakNrStart},9`,
          [],
          callback
      );
  }

  getAntArtiklerForside(callback: (status: string, data: string) => void){
    super.query(
      "SELECT COUNT(artikkelID) AS antall FROM artikkel WHERE viktighet=1",
      [],
      callback
    )
  };

  getAntArtiklerKategori(kategoriNavn: string,callback: (status: string, data: string) => void){
    super.query(
      "SELECT COUNT(artikkelID) AS antall FROM artikkel WHERE kategoriNavn=?",
      [kategoriNavn],
      callback
    )
  };

  getAlleArtikler(callback: (status: string, data: string) => void){
    super.query(
      "SELECT artikkelID,overskrift,kategoriNavn FROM artikkel ORDER BY overskrift",
      [],
      callback
    );
  }

  filtrerArtiklerGittKategoriOgOverskrift(kategoriNavn: string,sokeOrd: string, callback: (status: string, data: string) => void){
    super.query(
      "SELECT artikkelID, overskrift FROM artikkel WHERE kategoriNavn=? AND overskrift LIKE ?",
      [kategoriNavn,sokeOrd+'%'],
      callback
    )
  }

  getArtiklerGittKategori(sakNrStart: number,kategoriNavn: string,callback: (status: string, data: string) => void){
      super.query(
          `SELECT artikkelID,overskrift,antallLikesArtikkel, bildelink,kategoriNavn FROM artikkel WHERE kategoriNavn=? ORDER BY antallLikesArtikkel DESC,artikkelID DESC LIMIT ${sakNrStart},9`,
          [kategoriNavn],
          callback
      );
  }

  getKategorier(callback: (status: string, data: string) => void){
    super.query(
      "SELECT navn FROM kategori",
      [],
      callback
    );
  }

  getArtikkelID(artikkelID: number, callback: (status: string, data: string) => void){
      super.query(
          "SELECT artikkelID,overskrift, innhold,DATE_FORMAT(tidspunkt, '%Y-%m-%d %H:%i') AS tidspunkt,antallLikesArtikkel,bildelink,kategoriNavn,viktighet FROM artikkel WHERE artikkelID=?",
          [artikkelID],
          callback
      );
  }

  oppdaterLikesArtikkel(json: Object, artikkelID: number, callback: (status: string, data: string) => void){
    const oppdater = [json.antallLikesArtikkel, artikkelID];
    super.query(
      "UPDATE artikkel SET antallLikesArtikkel=? WHERE artikkelID=?",
      oppdater,
      callback
    );
  }

  //fikset
  opprettArtikkel(json: Object, callback: (status: string, data: string) => void){
      let artikkel = [json.overskrift, json.innhold, json.bildelink,json.viktighet, json.kategoriNavn];
      console.log("artikkel", artikkel);
      super.query(
          "INSERT INTO artikkel VALUES(DEFAULT,?,DEFAULT,?,?,?,0,?)",
          artikkel,
          callback
      );
  }

  lagKommentar(json: Object, callback: (status: string, data: string) => void){
    let kommentar = [json.kommentarNavn, json.innhold, json.nyhetssakID];
    console.log("kommentar", kommentar);
    super.query(
      "INSERT INTO kommentar VALUES(DEFAULT,?,?,?,DEFAULT,0)",
      kommentar,
      callback
    );
  }

  getKommentarerArtikkel(artikkelID: number,sorterEtterKolonne: string, sorteringsRekkefolge: string, callback: (status: string, data: string) => void){
    console.log("artikkelID",artikkelID);
    super.query(
      `SELECT kommentarID,kommentarNavn,innhold,DATE_FORMAT(tidspunkt, '%Y-%m-%d %H:%i') AS tidspunkt,antallLikesKommentar FROM kommentar WHERE artikkelID=? ORDER BY ${sorterEtterKolonne} ${sorteringsRekkefolge}`,
      [artikkelID],
      callback
    )
  }

  oppdaterLikesKommentar(json: Object, kommentarID: number, callback: (status: string, data: string) => void){
    const oppdater = [json.antallLikesKommentar, kommentarID];
    super.query(
      "UPDATE kommentar SET antallLikesKommentar=? WHERE kommentarID=?",
      oppdater,
      callback
    );
  }

  oppdaterArtikkel(json: Object,artikkelID: number, callback: (status: string, data: string) => void){
      const oppdatering = [json.overskrift, json.innhold, json.bildelink,json.viktighet, json.kategoriNavn,artikkelID];
      super.query(
          "UPDATE artikkel SET overskrift=?,innhold=?,bildelink=?,viktighet=?,kategoriNavn=? WHERE artikkelID=?",
          oppdatering,
          callback
      );
  }

  slettArtikkel(artikkelID: number, callback: (status: string, data: string) => void){
      super.query(
          "DELETE FROM artikkel WHERE artikkelID=?",
          [artikkelID],
          callback
      );
  }

  filtrerArtikler(overskrift: string,callback: (status: string, data: string) => void){
    console.log(overskrift);
    super.query(
      "SELECT artikkelID, overskrift FROM artikkel WHERE overskrift LIKE ?",
      [overskrift+'%'],
      callback
    )
  }

  getLiveFeed(callback: (status: string, data: string) => void){
    super.query(
      `SELECT artikkelID,overskrift,kategoriNavn, DATE_FORMAT(tidspunkt, '%Y-%m-%d %H:%i') AS tidspunkt FROM artikkel ORDER BY artikkelID DESC LIMIT 0,5`,
      [],
      callback
    )
  }
};