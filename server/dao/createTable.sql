DROP TABLE IF EXISTS kommentar;
DROP TABLE IF EXISTS artikkel;
DROP TABLE IF EXISTS kategori;

CREATE TABLE kategori(
  navn VARCHAR(35),
  CONSTRAINT kategori_pk PRIMARY KEY (navn)
);

CREATE TABLE artikkel(
  artikkelID INTEGER AUTO_INCREMENT,
  overskrift VARCHAR(45),
  tidspunkt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  innhold TEXT NOT NULL,
  bildelink TINYTEXT,
  viktighet TINYINT NOT NULL,
  antallLikesArtikkel INTEGER,
  kategoriNavn VARCHAR(35) NOT NULL,
  CONSTRAINT artikkel_pk PRIMARY KEY (artikkelID),
  CONSTRAINT artikkel_fk1 FOREIGN KEY (kategoriNavn) REFERENCES kategori(navn)
);

CREATE TABLE kommentar (
    kommentarID INTEGER AUTO_INCREMENT,
    kommentarNavn VARCHAR(30) NOT NULL,
    innhold TEXT NOT NULL,
    artikkelID INTEGER NOT NULL,
    tidspunkt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    antallLikesKommentar INTEGER NOT NULL,
    CONSTRAINT kommentar_pk PRIMARY KEY(kommentarID),
    CONSTRAINT kommentar_fk1 FOREIGN KEY(artikkelID) REFERENCES artikkel(artikkelID) ON DELETE CASCADE
)